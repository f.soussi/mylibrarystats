package fr.mylibrarystats;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class WebClientConfig {

  @Bean
  RestTemplate restTemplate() {
    return new RestTemplate();
  }
}