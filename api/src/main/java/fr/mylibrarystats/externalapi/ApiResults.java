package fr.mylibrarystats.externalapi;

import java.util.Collection;

import org.springframework.stereotype.Component;

@Component
public interface ApiResults {

  Collection<? extends ItemResults> getItems();
}