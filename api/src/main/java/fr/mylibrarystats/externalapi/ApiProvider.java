package fr.mylibrarystats.externalapi;

import org.springframework.stereotype.Component;

import fr.mylibrarystats.books.Book;

@Component
public interface ApiProvider {

  ApiResults getByAuthor(String author, Integer index);

  ApiResults getByISBN(String isbn);

  ApiResults getByAuthorAndTitle(String author, String title, Integer index);

  ApiResults getByTitle(String title, Integer index);

  Book addAdditionalInfo(Book book);

}
