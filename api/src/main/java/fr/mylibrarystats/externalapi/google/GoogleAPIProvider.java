package fr.mylibrarystats.externalapi.google;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.books.ISBN;
import fr.mylibrarystats.books.ISBNTypeEnum;
import fr.mylibrarystats.books.Images;
import fr.mylibrarystats.externalapi.ApiProvider;

@Component
public class GoogleAPIProvider implements ApiProvider {

  @Value("${google.books.api.url}")
  private String apiUrl;
  @Value("${google.books.api.key}")
  private String apiKey;
  @Autowired
  private RestTemplate restTemplate;

  public GoogleResults getByAuthor(String author, Integer index) {
    String uri = buildUri("inauthor:" + replaceSpace(author), index);
    return restTemplate.getForObject(uri, GoogleResults.class);
  }

  private String buildUri(String query, Integer index) {
    return UriComponentsBuilder
        .fromHttpUrl(apiUrl + "/books/v1/volumes/")
        .queryParam("q", query)
        .queryParam("startIndex", index)
        .queryParam("key", apiKey)
        .build(false) // Disable encoding
        .toUriString();
  }

  public GoogleResults getByISBN(String isbn) {
    String uri = buildUri("isbn:" + isbn, 0);
    return restTemplate.getForObject(uri, GoogleResults.class);

  }

  public GoogleResults getByTitle(String title, Integer index) {
    String uri = buildUri("intitle:" + replaceSpace(title), index);
    return restTemplate.getForObject(uri, GoogleResults.class);
  }

  public GoogleResults getByAuthorAndTitle(String author, String title, Integer index) {

    String uri = buildUri("intitle:" + replaceSpace(title) + "+inauthor:" + replaceSpace(author), index);
    return restTemplate.getForObject(uri, GoogleResults.class);
  }

  public String replaceSpace(String s) {
    return s.replaceAll(" ", "+");
  }

  @Override
  public Book addAdditionalInfo(Book book) {
    String isbn = getOneISBN(book.getIsbn());
    GoogleResults results = getByISBN(isbn);
    if (results == null || results.getItems() == null || results.getItems().isEmpty()) {
      Images images = getImages(isbn);
      book.setImages(images);
      return book;
    }
    results.getItems().stream().findFirst()
        .ifPresent(googleBook -> {
          if (googleBook.getBook().getImages() != null) {
            book.setImages(googleBook.getBook().getImages());
          } else {
            Images images = getImages(isbn);
            book.setImages(images);
          }
          book.setLanguage(googleBook.getBook().getLanguage());
          book.setDescription(googleBook.getBook().getDescription());
          book.setCategories(googleBook.getBook().getCategories());

        });
    return book;
  }

  private String getOneISBN(List<ISBN> isbn) {
    return isbn.stream()
        .filter(isbn1 -> (isbn1.getType().equals(ISBNTypeEnum.ISBN_13.getValue())
            || isbn1.getType().equals(ISBNTypeEnum.ISBN_10.getValue())))
        .findFirst()
        .orElseThrow(() -> new RuntimeException(
            ISBNTypeEnum.ISBN_13.getValue() + " or " + ISBNTypeEnum.ISBN_10.getValue() + " not found"))
        .getIdentifier();
  }

  private Images getImages(String isbn13) {
    Images images = new Images();
    images.setCover("http://covers.openlibrary.org/b/isbn/" + isbn13 + "-L.jpg");
    images.setSmallCover("http://covers.openlibrary.org/b/isbn/" + isbn13 + "-S.jpg");
    return images;
  }

}
