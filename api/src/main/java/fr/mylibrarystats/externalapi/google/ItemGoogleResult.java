package fr.mylibrarystats.externalapi.google;

import com.fasterxml.jackson.annotation.JsonAlias;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.externalapi.ItemResults;

public class ItemGoogleResult implements ItemResults {
  @JsonAlias("volumeInfo")
  private Book book;

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

}