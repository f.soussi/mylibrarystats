package fr.mylibrarystats.externalapi.google;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonAlias;

import fr.mylibrarystats.externalapi.ApiResults;

public class GoogleResults implements ApiResults {

  @JsonAlias("totalItems")
  private int nbResults;

  Collection<ItemGoogleResult> items;

  public int getNbResults() {
    return nbResults;
  }

  public void setNbResults(int nbResult) {
    this.nbResults = nbResult;
  }

  public Collection<ItemGoogleResult> getItems() {
    return items;
  }

}
