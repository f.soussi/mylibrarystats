package fr.mylibrarystats.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mylibrarystats.authentication.JwtAuthenticationResponse;
import fr.mylibrarystats.authentication.JwtTokenProvider;

@RestController
@RequestMapping("api/v1")
public class UserController {
  @Autowired
  private UserService userService;
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private JwtTokenProvider tokenProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  @PostMapping("/login")
  public ResponseEntity<?> login(@RequestBody AppUser appUser) {
    var user = userRepository.findByLogin(appUser.getUsername());
    if (user.isPresent() && passwordEncoder.matches(appUser.getPassword(), user.get().getPassword())) {
      authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(appUser.getUsername(),
              appUser.getPassword()));
      String jwt = tokenProvider.generateToken(appUser);
      return ResponseEntity.ok(new JwtAuthenticationResponse(appUser.getUsername(), jwt));
    } else {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
  };

  @PostMapping(value = "/users")
  public ResponseEntity<?> register(@RequestBody AppUser user) {
    userService.save(user);
    return ResponseEntity.ok().build();
  }
}
