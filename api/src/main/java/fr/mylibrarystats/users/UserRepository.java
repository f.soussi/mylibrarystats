package fr.mylibrarystats.users;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<AppUser, Long> {

  Optional<AppUser> findByLogin(String login);

}