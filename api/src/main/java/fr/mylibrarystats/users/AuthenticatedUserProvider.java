package fr.mylibrarystats.users;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthenticatedUserProvider {
  @Autowired
  private UserRepository userRepository;

  public String getUserName() {
    Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    if (principal instanceof UserDetails) {
      return ((UserDetails) principal).getUsername();
    } else {
      return "";
    }
  }

  public AppUser getUser() {

    String userName = getUserName();
    if (userName.isEmpty()) {
      throw new RuntimeException("User not connected");
    }
    Optional<AppUser> user = this.userRepository.findByLogin(userName);
    if (user.isPresent()) {
      return user.get();
    } else {
      throw new RuntimeException("User not found");
    }
  }
}
