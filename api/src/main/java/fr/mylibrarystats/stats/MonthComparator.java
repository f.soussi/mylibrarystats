package fr.mylibrarystats.stats;

import java.time.Month;
import java.util.Comparator;

class MonthComparator implements Comparator<String> {
  @Override
  public int compare(String month1, String month2) {
    return Month.valueOf(month1.toUpperCase()).compareTo(Month.valueOf(month2.toUpperCase()));
  }
}