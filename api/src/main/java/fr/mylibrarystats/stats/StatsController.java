package fr.mylibrarystats.stats;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.mylibrarystats.books.Book;

@RestController
@RequestMapping("api/v1")
public class StatsController {
  @Autowired
  private StatsService statsService;

  @GetMapping(value = "/stats/totalPages")
  public Integer getTotalPages(@RequestParam int year) {
    return statsService.getTotalPages(year);
  }

  @GetMapping(value = "/stats/totalBooks")
  public long getTotalBooks(@RequestParam int year) {
    return statsService.getTotalBooks(year);
  }

  @GetMapping(value = "/stats/totalAuthors")
  public long getTotalAuthors(@RequestParam int year) {
    return statsService.getTotalAuthors(year);
  }

  @GetMapping(value = "/stats/totalPublishers")
  public long getTotalPublishers(@RequestParam int year) {
    return statsService.getTotalPublishers(year);
  }

  @GetMapping(value = "/stats/mostReadPublisher")
  public String getMostReadPublishers(@RequestParam int year) {
    return statsService.getMostReadPublisher(year);
  }

  @GetMapping(value = "/stats/mostReadAuthor")
  public String getMostReadAuthors(@RequestParam int year) {
    return statsService.getMostReadAuthor(year);
  }

  @GetMapping(value = "/stats/numberOfBooksPerMonth")
  public Map<String, Integer> getNumberOfBooksPerMonth(@RequestParam int year) {
    return statsService.getNumberOfBooksPerMonth(year);
  }

  @GetMapping("/stats/numberOfPagesPerMonth")
  public Map<String, Integer> getNumberOfPagesPerMonth(@RequestParam int year) {
    return statsService.getNumberOfPagesPerMonth(year);
  }

  @GetMapping("/stats/longestBook")
  public Book getLongestBook(@RequestParam int year) {
    return statsService.getLongestBook(year);
  }

  @GetMapping("/stats/shortestBook")
  public Book getShortestBook(@RequestParam int year) {
    return statsService.getShortestBook(year);
  }
}
