package fr.mylibrarystats.stats;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.books.authors.Author;
import fr.mylibrarystats.books.publishers.Publisher;
import fr.mylibrarystats.books.users.UserBook;
import fr.mylibrarystats.books.users.UserBookRepository;
import fr.mylibrarystats.users.AuthenticatedUserProvider;

@Service
public class StatsService {

  @Autowired
  private AuthenticatedUserProvider authenticatedUserProvider;

  @Autowired
  private UserBookRepository userBookRepository;

  public int getTotalPages(int year) {
    List<UserBook> findAll = userBookRepository.findByUser(authenticatedUserProvider.getUser());
    return findAll.stream()
        .filter(filterSameYear(year))
        .map(UserBook::getBook)
        .mapToInt(Book::getNbPages)
        .sum();
  }

  public long getTotalBooks(int year) {
    return userBookRepository.findByUser(authenticatedUserProvider.getUser()).stream()
        .filter(filterSameYear(year))
        .count();
  }

  public long getTotalAuthors(int year) {
    return userBookRepository.findByUser(authenticatedUserProvider.getUser()).stream()
        .filter(filterSameYear(year))
        .map(UserBook::getBook)
        .flatMap(book -> book.getAuthors().stream())
        .collect(Collectors.toMap(Author::getName, Function.identity(), (existing, replacement) -> existing))
        .size();
  }

  public long getTotalPublishers(int year) {
    return userBookRepository.findByUser(authenticatedUserProvider.getUser()).stream()
        .filter(filterSameYear(year))
        .map(UserBook::getBook)
        .map(book -> book.getPublisher())
        .collect(Collectors.toMap(Publisher::getName, Function.identity(), (existing, replacement) -> existing))
        .size();
  }

  public String getMostReadPublisher(int year) {
    Map<Publisher, Long> publisherCount = userBookRepository.findByUser(authenticatedUserProvider.getUser()).stream()
        .filter(filterSameYear(year))
        .map(UserBook::getBook)
        .map(Book::getPublisher)
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    if (publisherCount.isEmpty()) {
      return "";
    }
    return Collections.max(publisherCount.entrySet(), Map.Entry.comparingByValue()).getKey().getName();
  }

  public String getMostReadAuthor(int year) {
    Map<Author, Long> authorCount = userBookRepository.findByUser(authenticatedUserProvider.getUser()).stream()
        .filter(filterSameYear(year))
        .map(UserBook::getBook)
        .flatMap(book -> book.getAuthors().stream())
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    if (authorCount.isEmpty()) {
      return "";
    }
    return Collections.max(authorCount.entrySet(), Map.Entry.comparingByValue()).getKey().getName();
  }

  public Map<String, Integer> getNumberOfBooksPerMonth(int year) {
    Map<String, Integer> numberOfBooksPerMonth = userBookRepository.findByUser(authenticatedUserProvider.getUser())
        .stream()
        .filter(filterSameYear(year))
        .map(userBook -> (userBook.getReadDate() != null) ? userBook.getReadDate() : userBook.getAddedDate())
        .collect(
            Collectors.groupingBy(date -> date.getMonth().toString(), TreeMap::new, Collectors.summingInt(date -> 1)));

    Map<String, Integer> sortedMap = new TreeMap<>(new MonthComparator());
    sortedMap.putAll(numberOfBooksPerMonth);
    return sortedMap;
  }

  private Predicate<? super UserBook> filterSameYear(int year) {
    return userBook -> userBook.getReadDate() != null && userBook.getReadDate().getYear() == year;
  }

  public Map<String, Integer> getNumberOfPagesPerMonth(int year) {
    Map<String, Integer> numberOfPagesPerMonth = userBookRepository.findByUser(authenticatedUserProvider.getUser())
        .stream()
        .filter(filterSameYear(year))
        .collect(Collectors.groupingBy(
            userBook -> ((userBook.getReadDate() != null) ? userBook.getReadDate() : userBook.getAddedDate()).getMonth()
                .toString(),
            TreeMap::new,
            Collectors.summingInt(userBook -> userBook.getBook().getNbPages())));
    Map<String, Integer> sortedMap = new TreeMap<>(new MonthComparator());
    sortedMap.putAll(numberOfPagesPerMonth);
    return sortedMap;
  }

  public Book getLongestBook(int year) {
    Optional<UserBook> userBook = userBookRepository
        .findFirstOrderByBookNbPagesDesc(authenticatedUserProvider.getUser(), year);
    if (!userBook.isPresent()) {
      return new Book();
    }
    return userBook.get().getBook();
  }

  public Book getShortestBook(int year) {
    Optional<UserBook> userBook = userBookRepository
        .findFirstOrderByBookNbPagesAsc(authenticatedUserProvider.getUser(), year);
    if (!userBook.isPresent()) {
      return new Book();
    }
    return userBook.get().getBook();
  }
}
