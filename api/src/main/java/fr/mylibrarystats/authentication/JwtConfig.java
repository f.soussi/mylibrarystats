package fr.mylibrarystats.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.mylibrarystats.users.UserService;

@Configuration
public class JwtConfig {
  @Autowired
  private UserService userDetailsService;

  public JwtConfig(UserService userDetailsService) {
    this.userDetailsService = userDetailsService;
  }

  @Bean
  public JwtTokenProvider jwtTokenProvider() {
    return new JwtTokenProvider(userDetailsService);
  }
}
