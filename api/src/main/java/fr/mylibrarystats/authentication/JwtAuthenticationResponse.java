package fr.mylibrarystats.authentication;

public class JwtAuthenticationResponse {
  private String accessToken;
  private String tokenType = "Bearer";
  private String login;

  public JwtAuthenticationResponse(String login, String accessToken) {
    this.accessToken = accessToken;
    this.login = login;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }
}
