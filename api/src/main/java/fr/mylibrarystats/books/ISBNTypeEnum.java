package fr.mylibrarystats.books;

public enum ISBNTypeEnum {
  ISBN_10("ISBN_10"),
  ISBN_13("ISBN_13");

  private final String value;

  ISBNTypeEnum(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}