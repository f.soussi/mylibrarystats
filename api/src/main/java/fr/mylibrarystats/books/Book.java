package fr.mylibrarystats.books;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

import fr.mylibrarystats.books.authors.Author;
import fr.mylibrarystats.books.publishers.Publisher;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class Book {
	@Id
	@GeneratedValue
	private Long id;

	@JsonAlias({ "kind" })
	private String kind;

	@JsonAlias({ "externalId" })
	private String externalId;

	@OneToMany(cascade = CascadeType.ALL)
	@JsonAlias({ "industryIdentifiers" })
	private List<ISBN> isbn;

	@JsonAlias({ "title" })
	private String title;

	@JsonAlias("subtitle")
	private String subtitle;

	@JsonAlias("authors")
	@ManyToMany
	@JoinTable(name = "book_authors", joinColumns = { @JoinColumn(name = "book_id") }, inverseJoinColumns = {
			@JoinColumn(name = "author_id") })
	private List<Author> authors;

	@JsonAlias("description")
	@Column(length = 5000)
	private String description;

	@JsonAlias("publishedDate")
	private String date;

	@JsonAlias("publisher")
	@ManyToOne
	@JoinColumn(name = "publisher_id")
	private Publisher publisher;

	@JsonAlias("categories")
	private List<String> categories;

	@JsonAlias("language")
	private String language;

	@JsonAlias("imageLinks")
	@OneToOne(cascade = CascadeType.ALL)
	private Images images;

	@JsonAlias("pageCount")
	private Integer nbPages;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public List<ISBN> getIsbn() {
		return isbn;
	}

	public void setIsbn(List<ISBN> isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Images getImages() {
		return images;
	}

	public void setImages(Images images) {
		this.images = images;
	}

	public Integer getNbPages() {
		return nbPages;
	}

	public void setNbPages(Integer nbPages) {
		this.nbPages = nbPages;
	}

}
