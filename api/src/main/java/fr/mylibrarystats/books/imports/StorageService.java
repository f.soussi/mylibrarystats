package fr.mylibrarystats.books.imports;

import java.nio.file.Path;

import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

  void init();

  void store(MultipartFile file);

  Path load(String filename);

  void deleteAll();
}
