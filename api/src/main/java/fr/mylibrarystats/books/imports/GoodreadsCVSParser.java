package fr.mylibrarystats.books.imports;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.books.ISBN;
import fr.mylibrarystats.books.ISBNTypeEnum;
import fr.mylibrarystats.books.authors.Author;
import fr.mylibrarystats.books.publishers.Publisher;
import fr.mylibrarystats.books.users.UserBook;
import fr.mylibrarystats.users.AuthenticatedUserProvider;

@Service
public class GoodreadsCVSParser implements CVSParser {

  public static final String TYPE = "text/csv";
  private StorageService fileStorageService;
  @Autowired
  private AuthenticatedUserProvider authenticatedUserProvider;
  private Collection<Book> rejectedBooks = new ArrayList<>();

  public GoodreadsCVSParser() {
    this.fileStorageService = new FileStorageService(new StorageProperties());
  }

  public Collection<Book> getRejectedBooks() {
    return this.rejectedBooks;
  }

  public Collection<UserBook> execute(MultipartFile file) throws URISyntaxException, ParseException {
    String csvFileName = getFile(file);
    CSVFormat csvFormat = CSVFormat.DEFAULT
        .builder()
        .setHeader(GoodreadHeaders.class)
        .setSkipHeaderRecord(true)
        .build();
    try (
        FileReader fileReader = new FileReader(csvFileName, StandardCharsets.UTF_8);
        CSVParser csvParser = new CSVParser(fileReader, csvFormat)) {
      return getUserBooks(csvParser);
    } catch (IOException e) {
      throw new RuntimeException("Error reading CSV file", e);
    }
  }

  private Collection<UserBook> getUserBooks(CSVParser csvParser) throws ParseException {
    Collection<UserBook> userBooks = new ArrayList<>();
    for (CSVRecord csvRecord : csvParser) {
      boolean wasReadThisYear = csvRecord.get(GoodreadHeaders.EXCLUSIVE_SHELF).equals("read") &&
          isCurrentYearOrLastYear(csvRecord);
      if (wasReadThisYear) {
        Optional<Book> book = createBook(csvRecord);
        if (book.isEmpty()) {
          continue;
        }
        userBooks.add(createUserBook(csvRecord, book.get()));
      }
    }
    return userBooks;
  }

  private UserBook createUserBook(CSVRecord csvRecord, Book book) {
    UserBook userBook = new UserBook();

    userBook.setUser(authenticatedUserProvider.getUser());

    userBook.setBook(book);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    if (!csvRecord.get(GoodreadHeaders.DATE_ADDED).isBlank()) {
      userBook.setAddedDate(LocalDate.parse(csvRecord.get(GoodreadHeaders.DATE_ADDED), dtf));
    }
    if (!csvRecord.get(GoodreadHeaders.DATE_READ).isBlank()) {
      userBook.setReadDate(LocalDate.parse(csvRecord.get(GoodreadHeaders.DATE_READ), dtf));
    }
    return userBook;
  }

  private Optional<Book> createBook(CSVRecord csvRecord) {
    Book book = new Book();
    book.setExternalId(csvRecord.get(GoodreadHeaders.BOOK_ID));
    book.setTitle(csvRecord.get(GoodreadHeaders.TITLE));
    book.setAuthors(getAuthors(csvRecord));
    book.setDate(csvRecord.get(GoodreadHeaders.YEAR_PUBLISHED));
    List<ISBN> isbn = getIndustrialIdentifiers(csvRecord);
    book.setPublisher(new Publisher(csvRecord.get(GoodreadHeaders.PUBLISHER)));
    book.setNbPages(getPageNumber(csvRecord));
    if (isbn.isEmpty()) {
      rejectedBooks.add(book);
      return Optional.empty();
    }
    book.setIsbn(isbn);
    return Optional.of(book);
  }

  private static boolean isCurrentYearOrLastYear(CSVRecord csvRecord) throws ParseException {
    String dateRead = csvRecord.get(GoodreadHeaders.DATE_READ);
    if (dateRead.isBlank()) {
      dateRead = csvRecord.get(GoodreadHeaders.DATE_ADDED);
    }
    LocalDate date = LocalDate.parse(dateRead, DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    return date.getYear() >= LocalDate.now().getYear() - 1;
  }

  private static Integer getPageNumber(CSVRecord csvRecord) {
    String pageNumber = csvRecord.get(GoodreadHeaders.NB_PAGES);
    return pageNumber.isBlank() ? 0 : Integer.valueOf(pageNumber);
  }

  private String getFile(MultipartFile file) {
    if (file.isEmpty()) {
      throw new RuntimeException("Failed to parse empty file.");
    }
    this.fileStorageService.store(file);
    Path path = this.fileStorageService.load(file.getOriginalFilename());
    return path.toString();
  }

  private List<Author> getAuthors(CSVRecord csvRecord) {
    String principalAuthor = csvRecord.get(GoodreadHeaders.AUTHOR);
    List<Author> authors = new ArrayList<>();
    Author author = new Author(principalAuthor);
    authors.add(author);
    return authors;
  }

  private List<ISBN> getIndustrialIdentifiers(CSVRecord csvRecord) {
    List<ISBN> allISBN = new ArrayList<>();
    Optional<ISBN> isbn13Object = createISBN(ISBNTypeEnum.ISBN_13.getValue(), csvRecord.get(GoodreadHeaders.ISBN13));
    if (isbn13Object.isPresent()) {
      allISBN.add(isbn13Object.get());
    }
    Optional<ISBN> isbnObject = createISBN(ISBNTypeEnum.ISBN_10.getValue(), csvRecord.get(GoodreadHeaders.ISBN));
    if (isbnObject.isPresent()) {
      allISBN.add(isbnObject.get());
    }
    return allISBN;
  }

  private Optional<ISBN> createISBN(String type, String value) {
    String simpleValue = value.replace("=\"", "").replace("\"", "");
    if (simpleValue.isBlank()) {
      return Optional.empty();
    }
    ISBN isbn = new ISBN();
    isbn.setIdentifier(simpleValue);
    isbn.setType(type);
    return Optional.of(isbn);
  }
}
