package fr.mylibrarystats.books.imports;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Collection;

import org.springframework.web.multipart.MultipartFile;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.books.users.UserBook;

public interface CVSParser {

  Collection<UserBook> execute(MultipartFile file) throws URISyntaxException, ParseException;

  Collection<Book> getRejectedBooks();

}
