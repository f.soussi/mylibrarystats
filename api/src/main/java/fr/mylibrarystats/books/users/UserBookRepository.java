package fr.mylibrarystats.books.users;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.users.AppUser;

public interface UserBookRepository extends JpaRepository<UserBook, Long> {

  List<UserBook> findByUser(AppUser user, Sort sort);

  List<UserBook> findByUser(AppUser user);

  Optional<UserBook> findByBookAndUser(Book book, AppUser user);

  @Query("SELECT ub FROM UserBook ub JOIN ub.book.isbn i WHERE ub.user = :user AND i.identifier = :isbn")
  Optional<UserBook> findByUserAndBookIsbn(@Param("user") AppUser user, @Param("isbn") String isbn);

  @Query("SELECT ub FROM UserBook ub WHERE ub.user = :user AND YEAR(ub.readDate) = :year ORDER BY ub.book.nbPages DESC, ub.book.title ASC LIMIT 1")
  Optional<UserBook> findFirstOrderByBookNbPagesDesc(@Param("user") AppUser user, @Param("year") int year);

  @Query("SELECT ub FROM UserBook ub WHERE ub.user = :user AND YEAR(ub.readDate) = :year AND ub.book.nbPages > 0 ORDER BY ub.book.nbPages ASC, ub.book.title ASC LIMIT 1")
  Optional<UserBook> findFirstOrderByBookNbPagesAsc(@Param("user") AppUser user, @Param("year") int year);
}
