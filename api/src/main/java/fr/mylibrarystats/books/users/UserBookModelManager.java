package fr.mylibrarystats.books.users;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.books.BookRepository;
import fr.mylibrarystats.books.ISBN;
import fr.mylibrarystats.books.ISBNTypeEnum;
import fr.mylibrarystats.books.authors.Author;
import fr.mylibrarystats.books.authors.AuthorService;
import fr.mylibrarystats.books.publishers.Publisher;
import fr.mylibrarystats.books.publishers.PublisherService;

@Component
public class UserBookModelManager {

  @Autowired
  private UserBookRepository userBookRepository;

  @Autowired
  private BookRepository bookRepository;

  @Autowired
  private PublisherService publisherService;

  @Autowired
  AuthorService authorService;

  public UserBook save(UserBook userBook) {
    savePublisher(userBook);
    saveAuthors(userBook);
    saveBook(userBook);
    saveUserBook(userBook);
    return userBook;
  }

  private void saveUserBook(UserBook userBook) {
    Optional<UserBook> existingUserBook = userBookRepository.findByBookAndUser(userBook.getBook(), userBook.getUser());
    if (existingUserBook.isEmpty()) {
      userBook = userBookRepository.save(userBook);
    }
  }

  private void saveAuthors(UserBook userBook) {
    List<Author> authors = authorService.save(userBook.getBook().getAuthors());
    userBook.getBook().setAuthors(authors);
  }

  private void savePublisher(UserBook userBook) {
    Publisher publisher = userBook.getBook().getPublisher();
    if (publisher != null) {
      publisher = publisherService.save(publisher);
      userBook.getBook().setPublisher(publisher);
    }
  }

  private void saveBook(UserBook userBook) {
    Book book = userBook.getBook();
    ISBN isbn = book.getIsbn().stream()
        .filter(a_isbn -> a_isbn.getType().equals(ISBNTypeEnum.ISBN_13.getValue())
            || a_isbn.getType().equals(ISBNTypeEnum.ISBN_10.getValue()))
        .findFirst().orElseThrow(() -> new RuntimeException("ISBN not found"));
    Optional<Book> existingBook = bookRepository.findByIsbn_Identifier(isbn.getIdentifier());
    if (existingBook.isEmpty()) {
      userBook.setBook(bookRepository.save(book));
    } else {
      userBook.setBook(existingBook.get());
    }
  }

}