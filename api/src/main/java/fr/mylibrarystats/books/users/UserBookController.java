package fr.mylibrarystats.books.users;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.mylibrarystats.books.Book;

@RestController
@RequestMapping("api/v1")
public class UserBookController {

  @Autowired
  private UserBookService userBookService;

  @GetMapping("/me/books")
  public ResponseEntity<Collection<UserBook>> getMyBooks(@RequestParam(required = false) String isbn) {
    Collection<UserBook> userBooks = new ArrayList<>();
    if (isbn != null && !isbn.isEmpty()) {
      Optional<UserBook> userBook = userBookService.getUserBookByIsbn(isbn);
      userBooks = userBook.isPresent() ? List.of(userBook.get()) : Collections.emptyList();
    } else {
      userBooks = userBookService.getMyBooks();
    }
    return ResponseEntity.ok(userBooks);
  }

  @PostMapping(value = "/me/books/file")
  public ResponseEntity<Map<String, Object>> getByGoodreadFile(@RequestParam("file") MultipartFile file)
      throws URISyntaxException, ParseException {
    return ResponseEntity.ok(userBookService.extractBooks(file));
  }

  @PostMapping(value = "/me/books")
  public ResponseEntity<UserBook> addBook(@RequestBody Book book) {
    return ResponseEntity.ok(userBookService.save(book));
  }

  @PatchMapping("/me/books/{id}")
  public ResponseEntity<UserBook> updateDates(@PathVariable Long id, @RequestBody Map<String, String> dates) {
    return ResponseEntity.ok(userBookService.update(id, dates));
  }

}
