package fr.mylibrarystats.books.users;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.books.imports.CVSParser;
import fr.mylibrarystats.externalapi.ApiProvider;
import fr.mylibrarystats.users.AppUser;
import fr.mylibrarystats.users.AuthenticatedUserProvider;
import jakarta.persistence.EntityNotFoundException;

@Service
public class UserBookService {
  @Autowired
  private UserBookRepository userBookRepository;

  @Autowired
  private AuthenticatedUserProvider authenticatedUserProvider;

  @Autowired
  private CVSParser parser;

  @Autowired
  private ApiProvider apiProvider;

  @Autowired
  private UserBookModelManager userBookModelManager;

  public Map<String, Object> extractBooks(MultipartFile file) throws URISyntaxException, ParseException {
    Collection<UserBook> userBooks = parser.execute(file);

    userBooks.forEach(
        userBook -> {
          saveAll(userBook);
        });
    Map<String, Object> booksInFile = new HashMap<>();
    booksInFile.put("importedBooks", userBooks);
    booksInFile.put("rejectedBooks", parser.getRejectedBooks());
    return booksInFile;
  }

  public Optional<UserBook> getUserBookByIsbn(String isbn) {
    AppUser user = authenticatedUserProvider.getUser();
    return userBookRepository.findByUserAndBookIsbn(user, isbn);
  }

  public Collection<UserBook> getMyBooks() {
    Sort sort = Sort.by(Sort.Direction.DESC, "modifiedDate");
    return userBookRepository.findByUser(authenticatedUserProvider.getUser(), sort);
  }

  private UserBook saveAll(UserBook userBook) {
    return userBookModelManager.save(userBook);
  }

  private Book addAdditionalInfo(Book book) {
    return apiProvider.addAdditionalInfo(book);
  }

  public UserBook save(Book book) {
    AppUser user = authenticatedUserProvider.getUser();
    UserBook userBook = new UserBook();
    userBook.setBook(addAdditionalInfo(book));
    userBook.setUser(user);
    userBook.setAddedDate(LocalDate.now());
    return saveAll(userBook);
  }

  public Optional<UserBook> get(Long id) {
    if (id == null) {
      throw new IllegalArgumentException("id cannot be null");
    }
    return userBookRepository.findById(id);
  }

  public UserBook update(long id, Map<String, String> dates) {
    UserBook userBook = this.get(id)
        .orElseThrow(() -> new EntityNotFoundException("UserBook with id " + id + " not found"));

    if (dates.containsKey("readDate")) {
      userBook.setReadDate(LocalDate.parse(dates.get("readDate").toString()));
    }
    if (dates.containsKey("addedDate")) {
      userBook.setAddedDate(LocalDate.parse(dates.get("addedDate").toString()));
    }
    userBook = userBookRepository.save(userBook);
    return userBook;
  }

}
