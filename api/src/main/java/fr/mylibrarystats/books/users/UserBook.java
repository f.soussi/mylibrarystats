package fr.mylibrarystats.books.users;

import java.time.LocalDate;
import java.time.LocalDateTime;

import fr.mylibrarystats.books.Book;
import fr.mylibrarystats.users.AppUser;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "book_id", "user_id" }))
public class UserBook {

  @Id
  @GeneratedValue
  private Long id;

  @ManyToOne
  private Book book;

  @ManyToOne(cascade = CascadeType.ALL)
  private AppUser user;

  private LocalDate readDate;

  private LocalDate addedDate;

  private LocalDateTime modifiedDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

  public AppUser getUser() {
    return user;
  }

  public void setUser(AppUser user) {
    this.user = user;
  }

  public LocalDate getReadDate() {
    return readDate;
  }

  public void setReadDate(LocalDate readDate) {
    this.readDate = readDate;
  }

  public LocalDate getAddedDate() {
    return addedDate;
  }

  public void setAddedDate(LocalDate addedDate) {
    this.addedDate = addedDate;
  }

  public LocalDateTime getModifiedDate() {
    return this.modifiedDate;
  }

  public void setModifiedDate(LocalDateTime modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  @PrePersist
  protected void onCreate() {
    this.modifiedDate = LocalDateTime.now();
  }

  @PreUpdate
  protected void onUpdate() {
    this.modifiedDate = LocalDateTime.now();
  }
}
