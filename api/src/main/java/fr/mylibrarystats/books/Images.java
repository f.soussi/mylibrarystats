package fr.mylibrarystats.books;

import com.fasterxml.jackson.annotation.JsonAlias;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Images {

	@Id
	@GeneratedValue
	private Long id;

	@JsonAlias("smallThumbnail")
	private String smallCover;

	@JsonAlias("thumbnail")
	private String cover;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSmallCover() {
		return smallCover;
	}

	public void setSmallCover(String smallCover) {
		this.smallCover = smallCover;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}
}
