package fr.mylibrarystats.books;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.mylibrarystats.externalapi.google.GoogleAPIProvider;
import fr.mylibrarystats.externalapi.google.GoogleResults;

@RestController
@RequestMapping("api/v1")
public class BooksController {

  @Autowired
  private GoogleAPIProvider googleAPIProvider;

  @GetMapping("/books")
  public GoogleResults get(
      @RequestParam(value = "author", required = false) String author,
      @RequestParam(value = "title", required = false) String title,
      @RequestParam(value = "index", defaultValue = "0") Integer index) {

    if (notEmpty(author) && notEmpty(title)) {
      return googleAPIProvider.getByAuthorAndTitle(author, title, index);
    }
    if (notEmpty(author)) {
      return googleAPIProvider.getByAuthor(author, index);
    }
    if (notEmpty(title)) {
      return googleAPIProvider.getByTitle(title, index);
    }

    return null;
  }

  private boolean notEmpty(String s) {
    return s != null && !s.isEmpty();
  }
}
