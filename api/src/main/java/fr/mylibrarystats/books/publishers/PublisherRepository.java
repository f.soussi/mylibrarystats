package fr.mylibrarystats.books.publishers;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {

  Optional<Publisher> findByNameIgnoreCase(String name);
}