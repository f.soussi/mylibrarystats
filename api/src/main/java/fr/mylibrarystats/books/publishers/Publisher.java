package fr.mylibrarystats.books.publishers;

import java.util.List;

import fr.mylibrarystats.books.Book;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Publisher {

  @Id
  @GeneratedValue
  private Long id;

  @Column(unique = true)
  private String name;

  @OneToMany(mappedBy = "publisher")
  private List<Book> books;

  public Publisher() {
  }

  public Publisher(String name) {
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}