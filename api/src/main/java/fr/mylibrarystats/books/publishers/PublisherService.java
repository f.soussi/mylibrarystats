package fr.mylibrarystats.books.publishers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublisherService {
  @Autowired
  private PublisherRepository publisherRepository;

  public Publisher save(Publisher publisher) {
    Optional<Publisher> existingPublisher = publisherRepository.findByNameIgnoreCase(publisher.getName());
    if (existingPublisher.isEmpty()) {
      return publisherRepository.save(publisher);
    } else {
      return existingPublisher.get();
    }
  }
}
