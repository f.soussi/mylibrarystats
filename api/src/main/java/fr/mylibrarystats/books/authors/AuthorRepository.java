package fr.mylibrarystats.books.authors;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {

  Optional<Author> findByNameIgnoreCase(String name);
}