package fr.mylibrarystats.books.authors;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {
  @Autowired
  private AuthorRepository authorRepository;

  public List<Author> save(List<Author> authors) {
    for (Author author : authors) {
      Optional<Author> authorExisting = authorRepository.findByNameIgnoreCase(author.getName());
      if (authorExisting.isEmpty()) {
        Author authorSaved = authorRepository.save(author);
        return List.of(authorSaved);
      } else {
        return List.of(authorExisting.get());
      }
    }
    return authors;
  }
}
